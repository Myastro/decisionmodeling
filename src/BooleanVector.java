import java.util.ArrayList;

public class BooleanVector extends ArrayList<Integer> {


    @Override
    public String toString() {
        String str = "";
        for(int i = 0 ; i < this.size(); i++) {
            str += this.get(i).toString();
            if( i == this.size() - 1 ) {
                str += "\n";
            } else {
                str += " ";
            }
        }

        return str;
    }

    public String toStringCSV() {
        String str = "";
        for(int i = 0 ; i < this.size(); i++) {
            str += this.get(i).toString();
            if( i == this.size() - 1 ) {
                str += "\n";
            } else {
                str += ";";
            }
        }

        return str;
    }
}
