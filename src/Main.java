
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOError;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static ArrayList<Integer> q5(Integer nbVar, ArrayList<CPT> cpts) {

        ArrayList<Integer> v = new ArrayList<>();
        for(int i = 0; i < nbVar; i++) {
            int tmp = (int) Math.floor(Math.random() * 100) % 2;
            v.add(tmp);
        }

        Double finalProba = getFullProba(cpts, v);

        System.out.println("finalProba : " + finalProba);

        if(Math.random() > finalProba) {
            return null;
        }

        return v;
    }

    public static Double getFullProba(ArrayList<CPT> cpts, ArrayList<Integer> v) {

        Double d = 1.0;
        for(CPT tmp: cpts) {
            d *= tmp.project(v);
        }

        return d;
    }

    public static ProblemDescriptor parseFile(String s) throws Exception {

        Scanner sc = new Scanner(new FileReader(s));

        ArrayList<CPT> cpts = new ArrayList<>();

        String l;

        l = sc.nextLine();

        Integer nbGlobVar = Integer.parseInt(l.split(" ")[1]);

        while(sc.hasNext()) {

            l = sc.nextLine();

            String [] ss = l.split(" ");

            int nbVar = Integer.parseInt(ss[0]);


            ArrayList<Integer> variables = new ArrayList<>();
            for(int i = 0; i < nbVar; i++) {
                variables.add(Integer.parseInt(ss[i + 1]));
            }

            CPT c = new CPT(variables);

            ArrayList<Double> probabilities = new ArrayList<>();
            for(int i = nbVar + 1 ; i < ss.length; i++) {
                probabilities.add(Double.parseDouble(ss[i]));
            }

            c.setProbabilities(probabilities);
            cpts.add(c);
        }


        return new ProblemDescriptor(nbGlobVar, cpts);
    }

    public static BooleanVector q10 (String filename) throws Exception {
        BooleanVector v = new BooleanVector();
        ProblemDescriptor pbd = parseFile(filename);
        ArrayList<Integer> tmpV;
        for(CPT c: pbd.getCpts()) {

            tmpV = new ArrayList<>(v);
            tmpV.add(1);
            Double p = c.project(tmpV);
            Integer x = Math.random() < p ? 1 : 0;

            v.add(x);
        }

        return v;
    }

    public static void main(String[] args) throws Exception {

        ArrayList<Integer> a = new ArrayList<>();

        a.add(0);
        a.add(1);

        CPT c = new CPT(a);

        ArrayList<Double> probabilities = new ArrayList<>();

        probabilities.add(0.1);
        probabilities.add(0.9);
        probabilities.add(0.2);
        probabilities.add(0.8);

        c.setProbabilities(probabilities);

        ArrayList<Integer> v = new ArrayList<>();

        v.add(0);
        v.add(0);
        v.add(1);

        Double p = c.project(v);

        System.out.println("Question 3 - " + p);

//      -- QUESTION 4

        ArrayList<CPT> cpts = new ArrayList<>();
        cpts.add(c);

        System.out.println("Question 4 - " + getFullProba(cpts, v));

//      -- QUESTION 5

        System.out.println("Question 5 - " + q5(4,  cpts));

//      -- QUESTION 6
//
//        ProblemDescriptor pbd = parseFile("./assets/asia.plain");
//
//
//        FileWriter fw = new FileWriter("result.txt");
//
//        String str;
//        ArrayList<Integer> ar;
//        for(int i= 0; i < 1000;) {
//
//            ar = q5(8, pbd.getCpts());
//            if(ar != null) {
//                fw.write(ar.toString().replace(",", "").replace('[', ' ').replace("]", "").substring(1));
//                if(i != 999) {
//                    fw.write("\n");
//                }
//
//                i++;
//            }
//
//        }
//
//        fw.close();

//      -- QUESTION 10

        for(int j = 0; j < 10; j ++) {


            FileWriter fw2 = new FileWriter("./results/result.win.q10-test-" + j + ".text");

            for (int i = 0; i < 1000; i++) {
                fw2.write(q10("./assets/win95pts.plain").toString());
            }

            fw2.close();
        }
    }

}
