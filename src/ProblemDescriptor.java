import java.util.ArrayList;

public class ProblemDescriptor {

    private Integer nbVar;
    private ArrayList<CPT> cpts;

    public ProblemDescriptor(Integer nbVar, ArrayList<CPT> cpts) {
        this.nbVar = nbVar;
        this.cpts = cpts;
    }

    public Integer getNbVar() {
        return nbVar;
    }

    public void setNbVar(Integer nbVar) {
        this.nbVar = nbVar;
    }

    public ArrayList<CPT> getCpts() {
        return cpts;
    }

    public void setCpts(ArrayList<CPT> cpts) {
        this.cpts = cpts;
    }
}
