import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class CPT{

    private ArrayList<Integer> variables;
    private Integer dimension;
    private ArrayList<Double> probabilities;

    public CPT(ArrayList<Integer> a) {

        this.dimension = a.size();

        this.variables = new ArrayList<>();
        this.variables.addAll(a);
    }

    public CPT(String s) {
        String[] ss = s.split(" ");

        int i = 1;

        this.variables = new ArrayList<>();
        for(; i < Integer.parseInt(ss[0]); i ++) {
            this.variables.add(Integer.parseInt(ss[i+1]));
        }

        this.dimension = this.variables.size();

        this.probabilities = new ArrayList<>();
        for(; i < ss.length; i ++) {
            this.probabilities.add(Double.parseDouble(ss[i]));
        }

        System.out.println("Probabilities : " + this.probabilities);
    }

    public ArrayList<Double> getProbabilities() {
        return probabilities;
    }

    public void setProbabilities(ArrayList<Double> probabilities) {

        this.probabilities = probabilities;
    }

    public Double getProbability(ArrayList<Integer> indexes) {
        int unidimensionnalIndex = 0;

        for (int i = 0; i < indexes.size(); i++) {
            unidimensionnalIndex += (i + 1) * indexes.get(i);
        }

        return this.probabilities.get(unidimensionnalIndex);
    }

    public Double project(ArrayList<Integer> v) {

        ArrayList<Integer> indexes = new ArrayList<>();
        for (Integer i : this.variables) {
            indexes.add(v.get(i));
        }

        return this.getProbability(indexes);

    }


    public Double getTrueProbability() {
        ArrayList<Integer> v = new ArrayList<>();

        for(int i  = 0; i < this.variables.size(); i++) {
            v.add(1);
        }

        return this.getProbability(v);
    }

    @Override
    public String toString() {

        String s = "";

        for (Integer i : variables) {
            s = s.concat(i.toString() + " ");
        }

        return s;
    }
}
